const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com'];

export function validate(email) {
  const ending = email.split('@')[1];
  return VALID_EMAIL_ENDINGS.includes(ending);
}
