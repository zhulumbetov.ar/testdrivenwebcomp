import { validate } from './email-validator.js';

const likeSection = document.getElementById('likeSection');


const join = document.getElementById('join');

document.body.onload = () => {
  join.style.backgroundImage = 'linear-gradient(to top, rgba(65, 65, 65, 0.5), rgba(65, 65, 65, 0.5)), url(./assets/images/joinourprogram.png)';
  join.style.height = "436px";
  join.style.display = 'flex';
  join.style.justifyContent = 'center';
  join.insertAdjacentHTML('afterbegin', '<h2 class="app-title" id="joinHeading">Join Our Program</h2>');
  join.insertAdjacentHTML('beforeend', '<p id="textunder">Sed do eiusmod tempor incididunt<br> ut labore et dolore magna aliqua.</p>');
  join.insertAdjacentHTML('beforeend', '<div id="rectangle"><input type="text" id="emailInput" placeholder="Email"></div>');
  join.insertAdjacentHTML('beforeend', '<button id="subscribeButton">Subscribe</button>');


  // like section
 
  
  function likeSectionFunc(show) {
    if(show) {
      likeSection.style.height = "850px";
    likeSection.style.display = 'flex';
    likeSection.style.backgroundColor = "white";
    likeSection.style.justifyContent = 'center';
  
    likeSection.insertAdjacentHTML('afterbegin', '<h2 class="app-title" id="likehead">Big Community of<br>People Like You </h2>');
  
    likeSection.insertAdjacentHTML('beforeend', '<p id="textunderlike">We’re proud of our products, and we’re really excited<br> when we get feedback from our users.</p>');
  
  
    const likehead = document.getElementById('likehead');
    likehead.style.color = "black";
    likehead.style.position = 'absolute';
    likehead.style.fontFamily = 'Oswald';
    likehead.style.fontWeight = 700;
    likehead.style.fontSize = '48px';
    likehead.style.lineHeight = '64px';
    likehead.style.marginTop = '90px';
  
    const textunderlike = document.getElementById('textunderlike');
    textunderlike.style.color = "#666666";
    textunderlike.style.position = 'absolute';
    textunderlike.style.fontFamily = 'Source Sans Pro';
    textunderlike.style.fontWeight = 400;
    textunderlike.style.fontSize = '24px';
    textunderlike.style.lineHeight = '32px';
    textunderlike.style.marginTop = '250px';
    textunderlike.style.textAlign = 'center';
    textunderlike.style.fontStyle = 'normal';
  
    const boxContainer = document.createElement('div');
  boxContainer.style.display = 'flex';
  boxContainer.style.justifyContent = 'space-between';
  boxContainer.style.position = 'absolute';
  boxContainer.style.width = '990px';
  boxContainer.style.height = '200px';
  boxContainer.style.padding = '10px';
  
  
  const box1 = document.createElement('div');
  box1.style.position = 'absolute';
  box1.style.width = '315px';
  box1.style.height = '400px';
  box1.style.background = 'white';
  box1.style.borderRadius = '8px';
  box1.style.display = 'flex';
  box1.style.flexDirection = 'column';
  box1.style.justifyContent = 'center';
  box1.style.alignItems = 'center';
  box1.style.marginTop = '360px';
  
  const girlImg = document.createElement('img');
  girlImg.style.width = '150px';
  girlImg.style.height = '150px';
  girlImg.style.objectFit = 'cover';
  box1.appendChild(girlImg);
  
  const text1 = document.createElement('p');
  text1.textContent = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.';
  text1.style.color = 'grey';
  text1.style.fontFamily = 'Source Sans Pro';
  text1.style.fontSize = '16px';
  text1.style.fontWeight = 400;
  text1.style.lineHeight = '20px';
  text1.style.letterSpacing = '0em';
  text1.style.textAlign = 'center';
  box1.appendChild(text1);
  
  const name1 = document.createElement('p');
  name1.style.color = '#464547';
  name1.style.fontFamily = 'Source Sans Pro';
  name1.style.fontSize = '12px';
  name1.style.fontWeight = 700;
  name1.style.lineHeight = '18px';
  name1.style.letterSpacing = '0.1em';
  name1.style.textAlign = 'center';
  box1.appendChild(name1);
  
  const position1 = document.createElement('p');
  position1.style.color = 'grey';
  position1.style.fontFamily = 'Source Sans Pro';
  position1.style.fontSize = '12px';
  position1.style.fontWeight = 400;
  position1.style.lineHeight = '16px';
  position1.style.letterSpacing = '0em';
  position1.style.textAlign = 'center';
  box1.appendChild(position1);
  fetch('http://localhost:3000/community')
    .then(response => response.json())
    .then(data => {
      // Assuming the data returned from the server is an array of community members
      const member = data.find(member => member.id === '2f1b6bf3-f23c-47e4-88f2-e4ce89409376');
      if (member) {
        name1.textContent = `${member.firstName} ${member.lastName}`.toUpperCase();
        position1.textContent = `${member.position}`;
        girlImg.src = `${member.avatar}`;
      }
    })
    .catch(error => console.error(error));
  
  const box2 = document.createElement('div');
  box2.style.position = 'absolute';
  box2.style.width = '315px';
  box2.style.height = '400px';
  box2.style.background = 'white';
  box2.style.borderRadius = '8px';
  box2.style.display = 'flex';
  box2.style.flexDirection = 'column';
  box2.style.justifyContent = 'center';
  box2.style.alignItems = 'center';
  box2.style.marginTop = '360px';
  box2.style.marginLeft = '335px'; // separation of 20px between box1 and box2
  
  
  
  const girlImg2 = document.createElement('img');
  girlImg2.style.width = '150px';
  girlImg2.style.height = '150px';
  girlImg2.style.objectFit = 'cover';
  box2.appendChild(girlImg2);
  
  const text2 = document.createElement('p');
  text2.textContent = 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.';
  text2.style.color = 'grey';
  text2.style.fontFamily = 'Source Sans Pro';
  text2.style.fontSize = '16px';
  text2.style.fontWeight = 400;
  text2.style.lineHeight = '20px';
  text2.style.letterSpacing = '0em';
  text2.style.textAlign = 'center';
  box2.appendChild(text2);
  
  const name2 = document.createElement('p');
  name2.style.color = '#464547';
  name2.style.fontFamily = 'Source Sans Pro';
  name2.style.fontSize = '12px';
  name2.style.fontWeight = 700;
  name2.style.lineHeight = '18px';
  name2.style.letterSpacing = '0.1em';
  name2.style.textAlign = 'center';
  box2.appendChild(name2);
  
  const position2 = document.createElement('p');
  position2.style.color = 'grey';
  position2.style.fontFamily = 'Source Sans Pro';
  position2.style.fontSize = '12px';
  position2.style.fontWeight = 400;
  position2.style.lineHeight = '16px';
  position2.style.letterSpacing = '0em';
  position2.style.textAlign = 'center';
  box2.appendChild(position2);
  fetch('http://localhost:3000/community')
    .then(response => response.json())
    .then(data => {
      // Assuming the data returned from the server is an array of community members
      const member = data.find(member => member.id === '1157fea1-8b72-4a9e-b253-c65fa1556e26');
      if (member) {
        name2.textContent = `${member.firstName} ${member.lastName}`.toUpperCase();
        position2.textContent = `${member.position}`;
        girlImg2.src = `${member.avatar}`;
      }
    })
    .catch(error => console.error(error));
  
  //
  const box3 = document.createElement('div');
  box3.style.position = 'absolute';
  box3.style.width = '315px';
  box3.style.height = '400px';
  box3.style.background = 'white';
  box3.style.borderRadius = '8px';
  box3.style.display = 'flex';
  box3.style.flexDirection = 'column';
  box3.style.justifyContent = 'center';
  box3.style.alignItems = 'center';
  box3.style.marginTop = '360px';
  box3.style.marginLeft = '685px'; // separation of 20px between box1 and box2
  
  const girlImg3 = document.createElement('img');
  girlImg3.style.width = '150px';
  girlImg3.style.height = '150px';
  girlImg3.style.objectFit = 'cover';
  box3.appendChild(girlImg3);
  
  const text3 = document.createElement('p');
  text3.textContent = 'Aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';
  text3.style.color = 'grey';
  text3.style.fontFamily = 'Source Sans Pro';
  text3.style.fontSize = '16px';
  text3.style.fontWeight = 400;
  text3.style.lineHeight = '20px';
  text3.style.letterSpacing = '0em';
  text3.style.textAlign = 'center';
  box3.appendChild(text3);
  
  const name3 = document.createElement('p');
  name3.style.color = '#464547';
  name3.style.fontFamily = 'Source Sans Pro';
  name3.style.fontSize = '12px';
  name3.style.fontWeight = 700;
  name3.style.lineHeight = '18px';
  name3.style.letterSpacing = '0.1em';
  name3.style.textAlign = 'center';
  box3.appendChild(name3);
  
  const position3 = document.createElement('p');
  position3.style.color = 'grey';
  position3.style.fontFamily = 'Source Sans Pro';
  position3.style.fontSize = '12px';
  position3.style.fontWeight = 400;
  position3.style.lineHeight = '16px';
  position3.style.letterSpacing = '0em';
  position3.style.textAlign = 'center';
  box3.appendChild(position3);

  fetch('http://localhost:3000/community')
    .then(response => response.json())
    .then(data => {
      // Assuming the data returned from the server is an array of community members
      const member = data.find(member => member.id === 'b96ac290-543c-4403-80fe-0c2d44e84ea9');
      if (member) {
        name3.textContent = `${member.firstName} ${member.lastName}`.toUpperCase();
        position3.textContent = `${member.position}`;
        girlImg3.src = `${member.avatar}`;
      }
    })
    .catch(error => console.error(error));
  
  boxContainer.appendChild(box1);
  boxContainer.appendChild(box2);
  boxContainer.appendChild(box3);
  
  likeSection.appendChild(boxContainer);

    }
    else
    {

    }
    
  }
  


  


  

  // likesection end
 
 

  const joinHeading = document.getElementById('joinHeading');
  joinHeading.style.color = "white";
  joinHeading.style.position = 'absolute';
  joinHeading.style.fontWeight = 700;
  joinHeading.style.marginTop = '87px';

  const joinText = join.querySelector('#textunder');
  textunder.style.position = 'absolute';
  textunder.style.fontFamily = "'Source Sans Pro'";
  textunder.style.fontStyle = 'normal';
  textunder.style.fontWeight = '400';
  textunder.style.fontSize = '24px';
  textunder.style.lineHeight = '32px';
  textunder.style.textAlign = 'center';
  textunder.style.color = 'rgba(255, 255, 255, 0.7)';
  textunder.style.marginTop = '176px';

  const rectangle = join.querySelector('#rectangle');
  rectangle.style.width = '400px';
  rectangle.style.height = '36px';
  rectangle.style.left = '307px';
  rectangle.style.top = '300px';
  rectangle.style.background = 'rgba(255, 255, 255, 0.15)';
  rectangle.style.marginTop = '306px';
  rectangle.style.color = '#FFFFFF';
  rectangle.style.fontFamily = 'Source Sans Pro';
  rectangle.style.fontStyle = 'normal';
  rectangle.style.fontWeight = '400';
  rectangle.style.fontSize = '14px';
  rectangle.style.lineHeight = '18px';
  rectangle.style.letterSpacing = '0.05em';
  rectangle.style.display = 'flex';
  rectangle.style.alignItems = 'center';
  rectangle.style.left = '100px';

  const emailInput = join.querySelector('#emailInput');
  emailInput.style.border = 'none';
  emailInput.style.flex = '1';
  emailInput.style.margin = '0';
  emailInput.style.padding = '0 10px';
  emailInput.style.background = 'transparent';
  emailInput.style.color = '#FFFFFF';
  emailInput.style.fontFamily = 'Source Sans Pro';
  emailInput.style.fontStyle = 'normal';
  emailInput.style.fontWeight = '400';
  emailInput.style.fontSize = '14px';
  emailInput.style.lineHeight = '18px';
  emailInput.style.letterSpacing = '0.05em';
 
  

  const subscribeButton = join.querySelector('#subscribeButton');
  if (subscribeButton) {
    subscribeButton.style.width = '111px';
    subscribeButton.style.height = '36px';
    subscribeButton.style.marginLeft = '30px';
    subscribeButton.style.marginTop = '306px';
    subscribeButton.style.background = '#000000'; 
    subscribeButton.style.borderRadius = '18px';
    subscribeButton.style.color = '#FFFFFF';
    subscribeButton.style.fontFamily = 'Oswald';
    subscribeButton.style.fontStyle = 'normal';
    subscribeButton.style.fontWeight = '400';
    subscribeButton.style.fontSize = '16px';
    subscribeButton.style.lineHeight = '26px';
    subscribeButton.style.textAlign = 'center';
    subscribeButton.style.letterSpacing = '0.1em';
    subscribeButton.style.border = 'none';
    subscribeButton.style.cursor = 'pointer';
    

const email = 'forbidden@gmail.com';
fetch('http://localhost:3000/subscribe', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({ email: email })
}).then(response => {
  if (response.ok) {
    console.log('Subscription successful!'); //ignore
  } else {
    console.error('Subscription failed!'); // ignore
  }
}).catch(error => {
  console.error('Error:', error); // ignore//
});

const isSubscribed = localStorage.getItem('isSubscribed');
if (isSubscribed) {
  // user is subscribed, update button text to "Unsubscribe"
  rectangle.style.display = 'none';
  subscribeButton.innerHTML = 'Unsubscribe';
  
}

// subscribeButton.disabled = true; subscribeButton.style.opacity = '0.5';//

subscribeButton.addEventListener('click', (event) => {
  event.preventDefault();
  let email = emailInput.value;
  const isSubscribed = localStorage.getItem('isSubscribed');
  if (isSubscribed) {
    subscribeButton.disabled = true; subscribeButton.style.opacity = '0.5';
    // user is subscribed, unsubscribe them
    const xhr = new XMLHttpRequest();
    const url = 'http://localhost:3000/unsubscribe';
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4 && xhr.status === 200) {
        console.log(JSON.stringify(xhr.responseText)); //after unsubscription ****************************
        subscribeButton.disabled = false; subscribeButton.style.opacity = '1';
      }
    };
    likeSectionFunc(false);
    const data = JSON.stringify({ email: email });
    xhr.send(data);

    localStorage.removeItem('isSubscribed');
    emailInput.value = '';
    rectangle.style.display = 'flex';
    subscribeButton.innerHTML = 'Subscribe';
    localStorage.removeItem('email');
  } else {
    if (email.includes('forbidden')) {
      fetch('http://localhost:3000/forbidden', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email })
      })
      .then(response => {
        if (response.status === 422) {
          return response.text().then(error => {
            throw new Error(error);
          });
        }
      })
      .catch(error => {
        window.alert(error.message);
      });
      email='123';
      emailInput.value = '';
    }
    const isValid = validate(email);
    if (isValid) {
      // make POST request to server
      subscribeButton.disabled = true; subscribeButton.style.opacity = '0.5';
      const xhr = new XMLHttpRequest();
      const url = 'http://localhost:3000/subscribe';
      xhr.open('POST', url, true);
      xhr.setRequestHeader('Content-Type', 'application/json');
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
          console.log(JSON.stringify(xhr.responseText)); //after subscription ****************************
          subscribeButton.disabled = false; subscribeButton.style.opacity = '1';
        } else if (xhr.readyState === 4 && xhr.status === 422) {
          window.alert('This email is not allowed.');
        }
      };
      const data = JSON.stringify({ email: email });
      xhr.send(data);
        localStorage.setItem('isSubscribed', true);


      rectangle.style.display = 'none';
      subscribeButton.innerHTML = 'Unsubscribe';
      localStorage.setItem('email', email);
      likeSectionFunc(true);
    } else {
      emailInput.value = '';
      // console.log('Email is invalid');
    }
  }
});


  
    
    
    
  }

  
  
  
  


};

